﻿
using UnityEngine;

public class Target : MonoBehaviour
{
    public float health = 50f;
    public int Points = 100;
    GameObject GameManager;
    Scoring scoring;
    //on prefabs change health of objects dependant on size / features idk

    private void Start()
    {
       GameManager = GameObject.Find("GameManager");
        scoring = GameManager.GetComponent<Scoring>();
    }
    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
        //introduce points system into here.
        //create variable for different prefabs so different asteroids are worth more points
        scoring.Score += Points;
    }
}
