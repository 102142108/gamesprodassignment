﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{
    public int Score;
    public Text timeText;
    public Text scoreText;
    float time;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliSeconds = (timeToDisplay % 1) * 1000;

        timeText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(Time.deltaTime);
        time += Time.deltaTime;
        DisplayTime(time);
        scoreText.text = Score.ToString();
    }
}
